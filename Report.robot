*** Settings ***
Resource    Firebase.robot
Resource    Grafana.robot
Resource    Sheets.robot
Suite Setup       Open Chrome
Suite Teardown    Close Chrome
Test Teardown    Run Keyword If Test Failed    Log Variables

*** Keywords ***
SetSheets
    ${GoogleRow}    Create List     G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z
    [Return]    @{GoogleRow} 
RunSheets
    [Arguments]     ${Row}    ${Target}
    Set Report    ${Row}    ${Target}
    Log To Console    SET activeUser ${Target} To ${Row}
Report9
    ${activeUser}      Get Active User
    RunSheets    C    ${activeUser}
Report12
    ${activeUser}      Get Active User
    RunSheets    D    ${activeUser}
Report17
    ${crashlytics}     Get Crashlytics
    Set Report    B    ${crashlytics}
    Log To Console    SET crashlytics ${crashlytics} To B
    
    ${activeUser}      Get Active User
    Set Report    E    ${activeUser}
    Log To Console    SET activeUser ${activeUser} To E
    
    ${events}          Get Events
    ${GoogleRow}    Create List     G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  AA  AB  AC  AD  AE  AF  AG  AH  AI  AJ  AK  AL  AM  AN  AO  AP  AQ  AR  AS  AT  AU  AV  AW  AX  AY  AZ  BA  BB  BC  BD  BE  BF  BG  BH  BI  BJ  BK  BL  BM  BN  BO  BP  BQ  BR  BS  BT  BU  BV  BW  BX  BY  BZ  CA  CB  CC  CD  CE  CF  CG  CH  CI  CJ  CK  CL  CM  CN  CO  CP  CQ  CR  CS  CT  CU  CV  CW  CX  CY  CZ  DA  DB  DC  DD  DE  DF  DG  DH  DI  DJ  DK  DL  DM  DN  DO  DP  DQ  DR  DS  DT  DU  DV  DW  DX  DY  DZ 

    FOR    ${num}    ${rowS}   IN ENUMERATE     @{GoogleRow}  
    RunSheets    ${rowS}    ${events[${num}]}
    Log To Console    Event ${num}
    Sleep    0.4s
    END

Report18
    ${activeUser}    Get Active User
    Set Report    E    ${activeUser}
    Log To Console    SET activeUser ${activeUser} To E

Report22
    ${crashlytics}     Get Crashlytics
    Set Report    B    ${crashlytics}
    Log To Console    SET crashlytics ${crashlytics} To B
    
    ${activeUser}      Get Active User
    Set Report    F    ${activeUser}
    Log To Console    SET activeUser ${activeUser} To F
    
    ${events}          Get Events
    ${GoogleRow}    Create List     G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  AA  AB  AC  AD  AE  AF  AG  AH  AI  AJ  AK  AL  AM  AN  AO  AP  AQ  AR  AS  AT  AU  AV  AW  AX  AY  AZ  BA  BB  BC  BD  BE  BF  BG  BH  BI  BJ  BK  BL  BM  BN  BO  BP  BQ  BR  BS  BT  BU  BV  BW  BX  BY  BZ  CA  CB  CC  CD  CE  CF  CG  CH  CI  CJ  CK  CL  CM  CN  CO  CP  CQ  CR  CS  CT  CU  CV  CW  CX  CY  CZ  DA  DB  DC  DD  DE  DF  DG  DH  DI  DJ  DK  DL  DM  DN  DO  DP  DQ  DR  DS  DT  DU  DV  DW  DX  DY  DZ 

    FOR    ${num}    ${rowS}   IN ENUMERATE     @{GoogleRow}  
    RunSheets    ${rowS}    ${events[${num}]}
    Log To Console    Event ${num}
    Sleep    0.4s
    END
    
    # Set Report    B    ${crashlytics}
    # Log To Console    SET crashlytics ${crashlytics} To B
    # Set Report    F    ${activeUser}
    # Log To Console    SET activeUser ${activeUser} To F
    # Set Report    G    ${events[0]}
    # Log To Console    SET events ${events[0]} To G
    # Set Report    H    ${events[1]}
    # Log To Console    SET events ${events[1]} To H
    # Set Report    I    ${events[2]}
    # Log To Console    SET events ${events[2]} To I
    # Set Report    J    ${events[3]}
    # Log To Console    SET events ${events[3]} To J
    # Set Report    K    ${events[4]}
    # Log To Console    SET events ${events[4]} To K
    # Set Report    L    ${events[5]}
    # Log To Console    SET events ${events[5]} To L
    # Set Report    M    ${events[6]}
    # Log To Console    SET events ${events[6]} To M
    # Set Report    N   ${events[7]}
    # Log To Console    SET events ${events[7]} To N
    #ReportGrafana
    
ReportGrafana
    Get Firebase Report Screen
    Get ADMD Screen
    Get Mobile-BE Screen
    Get System Monitor D01_1 Screen
    Get System Monitor D01_2 Screen
    
*** Test Cases ***
RunReport
    ${currentHour}  Get Time  hour
    ${currentHour}    Get Variable Value    ${setHour}    ${currentHour}
    ${currentHour}    Convert To Integer    ${currentHour}
    Log To Console    ${currentHour}
    @{time9}    Create List   ${08}    ${09}    ${10}
    @{time12}   Create List   ${11}    ${12}    ${13}
    @{time17}   Create List   ${16}    ${17}
    @{time18}   Create List   ${18}    ${19}
    @{time22}   Create List   ${21}    ${22}    ${23}
    Run Keyword If     ${currentHour} in ${time9}     Report9
    Run Keyword If     ${currentHour} in ${time12}    Report12
    Run Keyword If     ${currentHour} in ${time17}    Report17
    Run Keyword If     ${currentHour} in ${time18}    Report18
    Run Keyword If     ${currentHour} in ${time22}    Report22

Reportnn
    #ReportGrafana
    
    Report22
    #Cap Firebase


#robot -d Result -t RunReport -v sethour:22 -v sheetsname:FirebaseDailyReport Report.robot
#robot -d Result -t Reportnn report.robot