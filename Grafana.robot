*** Settings ***
Resource   Common.robot
Library    SeleniumLibrary    30s

*** Variables ***
${GRAFANA_URL}    http://grafana.siampiwat.in:3000/
${GRAFANA_USR}    siampiwat
${GRAFANA_PWD}    siampiwatview
${GRAFANA_SSPATH}    Grafana
${GRAFANA_WIDTH}     1366
${GRAFANA_HEIGHT}    868

*** Keywords ***
Set Timestamp
    ${date}    Get Time    year month day
    ${date}    Catenate    SEPARATOR=-    @{date}
    ${time}    Get Time    hour minute second
    ${time}    Catenate    SEPARATOR=-    @{time}
    Set Global Variable    ${GRAFANA_SSPATH}    ${GRAFANA_SSPATH}_${date}_${time}

Wait Until Graph And Table Are Visible
    Wait Until Element Is Visible    css=div.graph-panel__chart
    ${countGraph}    Get Element Count    css=div.graph-panel__chart
    FOR    ${interval}    IN RANGE    30
    Sleep    1s
    ${showGraph}    Get Element Count    css=div.graph-panel__chart canvas.flot-base
    Exit For Loop If    ${countGraph} == ${showGraph}
    END
    Wait Until Element Is Not Visible    css=div.table-panel-scroll.ng-hide

Get Offset Height
    ${navbar}    Execute Javascript    return document.getElementsByClassName('navbar')[0].offsetHeight
    ${container}    Execute Javascript    return document.getElementsByClassName('dashboard-container')[0].offsetHeight
    [Return]    ${${navbar} + ${container} + ${100}}

Login Grafana
    ${loginGrafana}    Get Variable Value    ${loginGrafana}    ${false}
    Return From Keyword If    ${loginGrafana} == ${true}
    Log To Console    [Login Grafana] Start
    Set Timestamp
    Open Chrome    ${GRAFANA_URL}
    Wait Until Element Is Visible    css=#login-view button
    Input Text    name=username    ${GRAFANA_USR}
    Log To Console    [Login Grafana] Input username
    Input Text    name=password    ${GRAFANA_PWD}
    Log To Console    [Login Grafana] Input password
    Click Element    css=#login-view button
    Log To Console    [Login Grafana] Submit login
    Wait Until Element Is Visible    css=div.dashboard-header
    Set Global Variable    ${loginGrafana}    ${true}
    Log To Console    [Login Grafana] Done

Get ADMD Screen
    Login Grafana
    Log To Console    [Get ADMD Screen] Start
    Set Window Size    ${GRAFANA_WIDTH}    ${GRAFANA_HEIGHT}
    Go To    http://grafana.siampiwat.in:3000/d/tgXr1X8mz/authentication-admd-dashboard?orgId=1
    Wait Until Graph And Table Are Visible
    ${height}    Get Offset Height
    Set Window Size    ${GRAFANA_WIDTH}    ${height}
    Capture Page Screenshot    ${GRAFANA_SSPATH}/ADMD.png
    Log To Console    [Get ADMD Screen] End

Get Mobile-BE Screen
    Login Grafana
    Log To Console    [Get Mobile-BE Screen] Start
    Set Window Size    ${GRAFANA_WIDTH}    ${GRAFANA_HEIGHT}
    Go To    http://grafana.siampiwat.in:3000/d/hcEfyX8iz/mobile-be-dashboard?orgId=1
    Wait Until Graph And Table Are Visible
    ${height}    Get Offset Height
    Set Window Size    ${GRAFANA_WIDTH}    ${height}
    Capture Page Screenshot    ${GRAFANA_SSPATH}/Mobile-BE.png
    Log To Console    [Get Mobile-BE Screen] End

Get System Monitor D01_1 Screen
    Login Grafana
    Log To Console    [Get System Monitor D01_1 Screen] Start
    Set Window Size    ${GRAFANA_WIDTH}    ${GRAFANA_HEIGHT}
    Go To    http://grafana.siampiwat.in:3000/d/000000012/zabbix-system-monitoring?orgId=1&var-group=Damocles&var-host=D01_1
    Wait Until Graph And Table Are Visible
    ${height}    Get Offset Height
    Set Window Size    ${GRAFANA_WIDTH}    ${height}
    Capture Page Screenshot    ${GRAFANA_SSPATH}/D01_1.png
    Log To Console    [Get System Monitor D01_1 Screen] End

Get System Monitor D01_2 Screen
    Login Grafana
    Log To Console    [Get System Monitor D01_2 Screen] Start
    Set Window Size    ${GRAFANA_WIDTH}    ${GRAFANA_HEIGHT}
    Go To    http://grafana.siampiwat.in:3000/d/000000012/zabbix-system-monitoring?orgId=1&var-group=Damocles&var-host=D01_2
    Wait Until Graph And Table Are Visible
    ${height}    Get Offset Height
    Set Window Size    ${GRAFANA_WIDTH}    ${height}
    Capture Page Screenshot    ${GRAFANA_SSPATH}/D01_2.png
    Log To Console    [Get System Monitor D01_2 Screen] End
    
Get Firebase Report Screen
    Login Grafana
    Log To Console    [Get Firebase Report Screen] Start
    Set Window Size    ${GRAFANA_WIDTH}    ${GRAFANA_HEIGHT}
    Go To    https://docs.google.com/spreadsheets/d/12s2lIKxptnOdkBUyfApN856IUPNej-CO0R3a3JrKlaw/edit#gid=1230677867
    ${promo-dismiss-link}    Run Keyword And Return Status    Element Should Be Visible    id=promo-dismiss-link
    Run Keyword If    ${promo-dismiss-link}    Click Element    id=promo-dismiss-link
    Capture Page Screenshot    ${GRAFANA_SSPATH}/Firebase.png
    Log To Console    [Get Firebase Report Screen] End
    