*** Settings ***
Resource   Common.robot
Library    Collections
Library    SeleniumLibrary    30s

*** Variables ***
${FIREBASE_URL}    https://console.firebase.google.com/project/onesiam-mobile
${FIREBASE_USR}    Karthoke.Tester@gmail.com
${FIREBASE_PWD}    TEPHUDSADIN999
${FIREBASE_TEL}    0949861995

*** Keywords ***
Get Today Epoch
    ${y}    ${m}    ${d}    Get Time    year,month,day
    ${epoch}    Get Time    epoch    ${y}-${m}-${d} 00:00:00
    ${begin}    Set Variable    ${epoch}000
    ${end}    Set Variable    ${epoch+86399}000
    [Return]    ${begin}    ${end}

Get Actually Text
    [Arguments]    ${locator}    ${iframe}=
    ${condition}    Evaluate    '${iframe}' != ''
    Run Keyword If    ${condition}    Select Frame    ${iframe}
    Wait Until Element Is Visible    ${locator}
    FOR    ${interval}    IN RANGE    10
    Sleep    1s
    Wait Until Element Is Visible    ${locator}
    ${text}    Get Text    ${locator}
    Log To Console    [Get Text] #${${interval} + ${1}} - ${text}
    Run Keyword And Ignore Error    Exit For Loop If    ${text} > 0
    END
    Run Keyword If    ${condition}    Unselect Frame
    [Return]    ${text}

Redirect
    [Arguments]    ${url}
    FOR    ${interval}    IN RANGE    30
    Go To    ${url}
    ${condition}    Run Keyword And Return Status    Location Should Contain    ${url}
    Exit For Loop If    ${condition}
    END

Login Firebase
    ${loginFirebase}    Get Variable Value    ${loginFirebase}    ${false}
    Return From Keyword If    ${loginFirebase} == ${true}
    Log To Console    [Login Firebase] Start
    Open Chrome    ${FIREBASE_URL}
    ${locator_email}       Set Variable    css=#identifierId
    ${locator_password}    Set Variable    css=#password input
    ${locator_next}        Set Variable    css=#identifierNext
    ${locator_signIn}      Set Variable    css=#passwordNext

    Wait Until Element Is Visible    ${locator_email}
    Input Text    ${locator_email}    ${FIREBASE_USR}
    Press Keys    ${locator_email}    RETURN
    # Click Element    ${locator_next}
    Wait Until Element Is Not Visible    ${locator_next}
    Log To Console    [Login Firebase] Input username

    Wait Until Element Is Visible    ${locator_password}
    Input Password    ${locator_password}    ${FIREBASE_PWD}
    Press Keys    ${locator_password}    RETURN
    # Click Element    ${locator_signIn}
    Wait Until Element Is Not Visible    ${locator_signIn}
    Log To Console    [Login Firebase] Input password
    
    ${phoneNumberId}    Set Variable    css=#phoneNumberId
    ${isVisiblePhoneNumberId} 	Run Keyword And Return Status     Wait Until Element Is Visible    ${phoneNumberId}
    Run Keyword If    ${isVisiblePhoneNumberId}    Input Text    ${phoneNumberId}    ${FIREBASE_TEL}
    Run Keyword If    ${isVisiblePhoneNumberId}    Press Keys    ${phoneNumberId}    RETURN
    Run Keyword If    ${isVisiblePhoneNumberId}    Wait Until Element Is Not Visible    ${phoneNumberId}
    Log To Console    [Login Firebase] Input phonenumber
    
    Wait Until Element Is Visible    css=fb-navbar    1m
    Set Global Variable    ${loginFirebase}    ${true}
    Log To Console    [Login Firebase] Done

Get Active User
    Log To Console    [Get Active User] Start
    Login Firebase
    ${today}    Get Today Epoch
    Redirect    https://analytics.google.com/analytics/app/mobile/?authuser=0&hl=th&fpn=828751992199#/p182686739/mobile/overview?fpn=828751992199&swu=1&sgu=1&sus=not_upgraded&t=${today[0]}
    ${activeUser}    Get Actually Text    css=.counter
    Set Global Variable    ${activeUser}
    Log To Console    [Get Active User] ${activeUser} Done 
    [Return]    ${activeUser}

Get Crashlytics
    Log To Console    [Get Crashlytics] Start
    Login Firebase
    ${today}    Get Today Epoch
    Redirect    ${FIREBASE_URL}/crashlytics/app/android:com.onesiam.siampiwat/issues?type=crash&state=open&time=${today[0]}:${today[1]}
    ${crashlytics}    Get Actually Text    css=.crashes .value
    Set Global Variable    ${crashlytics}
    Log To Console    [Get Crashlytics] ${crashlytics} Done
    [Return]    ${crashlytics}


Get Events
    Log To Console    [Get Events] Start
    Login Firebase
    ${today}    Get Today Epoch
    #${searchEvents}    Create List    app_remove    open_app    profile_edit_click    profile_view    profile_card_change    profile_interest_change    register_viz    first_open
    ${searchEvents}    Create List    app_clear_data    app_exception    app_remove    app_update    calendar_content_view    calendar_date_change    calendar_filter_mall_selected    calendar_month_change    calendar_qrcode_click    calendar_search	calendar_view    change_mall    contact_mall_change    contact_view    coverpage_click    coverpage_show    directory_facility_click  directory_fliter  directory_floor_change  directory_mall_change  directory_map_view  directory_poi_view  directory_search  directory_view  dynamictheme_setting_view  event_choose_language  event_choose_language_next  event_demographic  event_demographic_home  event_demographic_next  event_lifestyle_catagory_next  event_lifestyle_confirm_edit  event_lifestyle_confirm_next  event_lifestyle_keyword_next  event_lifestyle_keyword_topic  event_login_skip  event_login_success  event_policy_cancel  event_polity_accept  event_viz_card_home  event_viz_card_next  event_viz_confirm_home  event_viz_confirm_next  event_viz_id_home  event_viz_id_next  event_viz_otp_home  event_viz_otp_send  first_open  global_search  highlights_content_view  highlights_popular_click  highlights_qrcode_click   highlights_recent_click  highlights_search  highlights_view  home_event_view  home_highlights_view  home_login_click   home_notifications_content_view  home_notifications_group_change  home_notifications_inbox  home_promotion_view   home_qrcode_click  landing_choose_language  landing_demographic  landing_introduce  Landing_introduce  landing_lifestyle_catagory  landing_lifestyle_confirm  landing_lifestyle_keyword  landing_login  landing_policy  landing_register_success  landing_viz_card  landing_viz_confirm  landing_viz_id  Landing_viz_id  landing_viz_otp  luckydraw_content_join  luckydraw_content_tab_announcements   luckydraw_content_tab_conditions  luckydraw_content_view  luckydraw_qrcode_click  luckydraw_search  luckydraw_tab_activities  luckydraw_tab_activities  luckydraw_tab_joined  luckydraw_view  notification_setting_view  notification_view  open_app  os_update  privilege_qrcode_click  privilege_view  privileges_category  privileges_content_redeem  privileges_content_tab_branches  privileges_content_tab_conditions  privileges_content_view  privileges_coupon_use  privileges_coupon_view  privileges_history_click  privileges_qrcode_click  privileges_search  privileges_view  profile_card_change  profile_edit_click  profile_interest_change  profile_view  register_viz  screen_view  session_start  setting_automatic_theme  setting_laguage_change  setting_logout_click  setting_notifications  setting_theme_change  setting_tutorial_click  setting_view  splash_screen_jailbreak_close  splash_screen_jailbreak_close_click  splash_screen_jailbreak_retry  splash_screen_retry  splash_screen_retry_click
    ${events}    Create List
    FOR    ${searchEvent}    IN    @{searchEvents}
    Log To Console    [Get Events] ${searchEvent}
    Redirect    https://analytics.google.com/analytics/app/mobile/?authuser=0&hl=th&fpn=828751992199#/p182686739/m/events/overview?t=${today[0]}&fpn=828751992199&sus=not_upgraded&sgu=1&swu=1&params=_r..layout.pageNumber%3D0%26_u..pageSize%3D25%26_u.dateOption%3Dtoday%26_r..layout.searchTerm%3D${searchEvent}
    ${hasEventName}    Run Keyword And Return Status     Wait Until Element Contains    css=.event-name    ${searchEvent}
    ${event}    Run Keyword If    ${hasEventName}    Get Actually Text    xpath=(//*[@class="data ng-binding"])[1]    ELSE    Set Variable    null
    


    Log To Console    [Events]  ${event} 
    Append To List    ${events}    ${event}
    END
    Set Global Variable    ${events}
    Log To Console    [Get Events] Done
    [Return]    ${events}

